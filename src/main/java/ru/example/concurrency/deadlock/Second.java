package ru.example.concurrency.deadlock;

public class Second {
    synchronized void action(First a) {

        String name = Thread.currentThread().getName();
        System.out.println(name + " вошел в метод Second.action()");

        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("Класс Second прерван");
        }

        System.out.println(name + " пытается вызвать метод First.last()");
        a.last();
    }

    synchronized void last() {
        System.out.println("В методе Second.last()");
    }
}
