package ru.example.concurrency.deadlock;

public class First {
    synchronized void action(Second b) {

        String name = Thread.currentThread().getName();
        System.out.println(name + " вошел в метод First.action()");

        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("Класс First прерван");
        }

        System.out.println(name + " пытается вызвать метод Second.last()");
        b.last();
    }

    synchronized void last() {
        System.out.println("В методе First.last()");
    }
}
