package ru.example.concurrency.deadlock;

public class Main implements Runnable{
    First a = new First();
    Second b = new Second();

    Main() {
        Thread.currentThread().setName("Главный поток");
        Thread t = new Thread(this, "Соперничающий поток");
        t.start();

        a.action(b); // получить блокировку для объекта a
        // в этом потоке исполнения

        System.out.println("Назад в главный поток");
    }

    public void run() {
        b.action(a); // получить блокировку для объекта b
        // в другом потоке исполнения
        System.out.println("Назад в другой поток");
    }

    public static void main(String args[]) {
        new Main();
        //Основная проблема в том что 2 объекта связаны, но блокировку на объекты получают независимо.
        // Значит можно использовать перед вызовом некий общий монитор который будет разрешать запускать метод
        // только если не был залочен другой объект.
    }
}
