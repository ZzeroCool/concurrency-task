package ru.example.concurrency.starttogether;

import java.time.LocalTime;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Phaser;

public class Activity implements Runnable{
    private static int count = 0;
    private LocalTime startTime;
    private StrategyType strategy;
    private CyclicBarrier barrier;
    private CountDownLatch cdl;
    private Phaser phaser;

    public Activity(CyclicBarrier barrier) {
        this.barrier = barrier;
        strategy = StrategyType.CyclicBarrier;
    }

    public Activity(CountDownLatch cdl) {
        this.cdl = cdl;
        strategy = StrategyType.CountDownLatch;
    }

    public Activity(Phaser phaser) {
        this.phaser = phaser;
        phaser.register();
        strategy = StrategyType.Phaser;
    }

    public void start() {
        startTime = LocalTime.now();
        LocalTime finishTime = startTime.plusMinutes(1);
        String name = Thread.currentThread().getName();
        System.out.println("Thread " + name + " started in " + startTime + " and will finish in " + finishTime);
        try {
            System.out.println("Thread " + name + " suspend " + LocalTime.now());
            switch (strategy) {
                case CyclicBarrier -> {
                    barrier.await();
                    System.out.println("barrier number waiting is: " + barrier.getNumberWaiting());
                }
                case CountDownLatch -> {
                    cdl.countDown();
                    System.out.println("cdl count is: " + cdl.getCount());
                    cdl.await();
                }
                case Phaser -> {
                    phaser.arriveAndAwaitAdvance();
                    System.out.println("Thread " + name + " Phase " + phaser.getPhase() + " starting...");
                    phaser.arriveAndDeregister();
                }
            }

            System.out.println("Thread " + name + " wake up " + LocalTime.now());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        while (finishTime.isAfter(LocalTime.now())) {
            LocalTime currentTime = LocalTime.now();
            if (currentTime.getNano() == 0) {
                ++count;
            } else {
                for (int i = 0; i < 100; i++) {
                    int useless = name.split("e").length - new Random().nextInt();
                }
            }
        }

        System.out.println("In thread " + name + " count is :" + count);
    }

    @Override
    public void run() {
        start();
    }

    enum StrategyType{
        CyclicBarrier, CountDownLatch, Phaser
    }
}
