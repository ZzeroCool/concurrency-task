package ru.example.concurrency.starttogether;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {

    public static void main(String[] args) {
        List<Thread> threads = new ArrayList<>();
        CountDownLatch cdl = new CountDownLatch(15);

        for (int i = 0; i < 16; i++) {
            threads.add(new Thread(new Activity(cdl), "Thread " + i));
        }

        threads.forEach(t -> t.start());
    }
}
