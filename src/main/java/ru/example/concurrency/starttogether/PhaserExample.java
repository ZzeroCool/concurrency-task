package ru.example.concurrency.starttogether;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;

public class PhaserExample {

    public static void main(String[] args) {
        List<Thread> threads = new ArrayList<>();
        Phaser phaser = new Phaser(1);

        for (int i = 0; i < 16; i++) {
            threads.add(new Thread(new Activity(phaser), "Thread " + i));
        }
        threads.forEach(t -> t.start());
        phaser.arriveAndAwaitAdvance();
        System.out.println("Старт фазы 1");
        phaser.arriveAndDeregister();
    }
}
