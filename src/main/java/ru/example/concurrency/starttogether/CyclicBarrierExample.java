package ru.example.concurrency.starttogether;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample {

    public static void main(String[] args) {
        List<Thread> threads = new ArrayList<>();
        CyclicBarrier cb = new CyclicBarrier(15);

        for (int i = 0; i < 16; i++) {
            threads.add(new Thread(new Activity(cb), "Thread " + i));
        }

        threads.forEach(t -> t.start());
    }
}
