package ru.example.concurrency.starvation;

public class ObjectExample implements Runnable{

    private final Object resource;
    private final String message;
    private final boolean fair;

    public ObjectExample(Object resource, String message, boolean fair) {
        this.resource = resource;
        this.message = message;
        this.fair = fair;
    }

    @Override
    public void run() {
        synchronized (this) {
            while (true){
                synchronized (resource) {
                    print(message);
                    try {
                        (fair ? resource : this).wait(100);
                    } catch (InterruptedException ignored) {}
                }
            }
        }
    }

    private static void print(String str) {
        synchronized (System.out) {
            System.out.print(str);
            System.out.flush();
        }
    }
}
