package ru.example.concurrency.starvation;

public class Main {
    public static void main(String[] args) {

        boolean fair = false;

        // get the number of available CPUs, do twice as much threads.
        final int cpus = Runtime.getRuntime().availableProcessors();
        System.out.println("" + cpus + " available CPUs found");
        final int runners = cpus * 2;
        System.out.println("starting " + runners + " runners");

        final Object resource = new Object();

        // create sample runners and start them
        for (int i = 1; i <= runners; i++) {
            (new Thread(new ObjectExample(resource, String.valueOf(i), fair))).start();
        }

        // suspend main thread
        synchronized (ObjectExample.class) {
            try {
                ObjectExample.class.wait();
            } catch (InterruptedException ignored) {
            }
        }
    }
}
