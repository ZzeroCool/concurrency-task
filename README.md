# concurrency-task

concurrency task for training

## Starvation

Задача демонстрирует голодание при использовании синхронизации на объекте. 
Для решения данной проблемы `(fair = true)` можно использовать общий ресурс, который позволит "честнее" использовать 
процессорное время. 


## Deadlock
Задача демонстрирует возможность получения взаимной блокировки. Истоки проблемы в том, что объекты
First и Second связаны, а значит у них должна быть общая блокировка. 
Подобное поведение можно достигнуть несколькими способами - использованием сторонней сущности для блокировки. 
В случае использования блокировки на одном и том же классе - может быть определен
порядок по которому блокировка осуществляется. 


## Start together
Задача создания одновременно 15 потоков, которые приостанавливаются а затем стартуют одновременно.
Для решения данной задачи использовались такие классы синхронизации как CyclicBarrier, CountDownLatch, Phaser.
После создания потоков Activity определяет по какой стратегии будут работать потоки и запускает нужный
механизм. Потоки висят в цикле на протяжении 1 минуты каждый раз проверяя время когда значение миллисекунд
равно 0 и увеличивает счетчик. По прошествии 1 минуты выдает результат.